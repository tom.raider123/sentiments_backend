const router = require('express').Router()
const userCtrl = require('../controllers/userCtrl')
const auth = require('../middleware/auth')
const authAdmin = require('../middleware/authAdmin')

router.post('/register', userCtrl.register)

router.post('/activation', userCtrl.activateEmail)

router.post('/login', userCtrl.login)

router.post('/refresh_token', userCtrl.getAccessToken)

router.post('/forgot', userCtrl.forgotPassword)

router.post('/reset', auth, userCtrl.resetPassword)

router.get('/infor', auth, userCtrl.getUserInfor)

router.get('/all_infor', auth, authAdmin, userCtrl.getUsersAllInfor)

router.get('/logout', userCtrl.logout)

router.patch('/update/:id', auth, userCtrl.updateUser)

router.patch("/update_avatar/:id", auth, userCtrl.updateAvatar);

router.patch('/update_role/:id', auth, authAdmin, userCtrl.updateUsersRole)

router.delete('/delete/:id', auth, authAdmin, userCtrl.deleteUser)

router.get('/dashboard_info/:id', userCtrl.userDashboard)

// Social Login
router.post('/google_login', userCtrl.googleLogin)
router.post('/facebook_login', userCtrl.facebookLogin)
//user upload
router.post('/upload_files/:id', auth, userCtrl.userUpload)
router.get('/get_files/', userCtrl.getUpload)
router.get('/get_user_batches/:id',userCtrl.getUserBatches)
router.get('/get_batch_files/:id',userCtrl.getBatchFiles)
router.post('/validatebatchname/', userCtrl.validateBatchNameFileUpload)
// router.get('/getFile/:id', userCtrl.getFile)


module.exports = router
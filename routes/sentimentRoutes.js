const express = require('express');
const router = express.Router();

const textToSentimentsCtrl = require('../controllers/textToSentiments');

router.post('/sentiment/',textToSentimentsCtrl.convertToSentiments);
router.get('/getFile/:id',textToSentimentsCtrl.getFile);
router.get('/getTranslation/:id',textToSentimentsCtrl.getTranslation);

module.exports = router
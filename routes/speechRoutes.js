const express = require('express');
const router = express.Router();

// const speechToText = require('../middleware/speechToText');
const speechToTextCtrl = require('../controllers/speechToTextCtrl');

router.post('/speech/',speechToTextCtrl.convertToText);

module.exports = router
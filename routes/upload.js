const express = require('express');
const router = express.Router();
const  Multer = require('multer');
const multer = Multer({
    storage: Multer.memoryStorage(),
    limits: {
      fileSize: 20 * 1024 * 1024, // no larger than 5mb, you can change as needed.
    },
  });
const uploadImage = require('../middleware/uploadImage');
const uploadAudio = require('../middleware/uploadAudio');
const uploadCtrl = require('../controllers/uploadCtrl');
const auth = require('../middleware/auth');

router.post('/upload_avatar',multer.single('file'), uploadImage, uploadCtrl.uploadAvatar);
router.post('/upload_audio_cloud', multer.array('file',10), uploadAudio, uploadCtrl.uploadAudio);

module.exports = router
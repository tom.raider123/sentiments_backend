require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser');
const cors = require('cors')
const cookieParser = require('cookie-parser')
const fileUpload = require('express-fileupload')
const path = require('path')
const schedule = require('node-schedule');
const app = express()
app.use(express.json())
app.use(cors())
app.use(cookieParser())
// app.use(fileUpload({
//     useTempFiles: true
// }))
// const speechCrone = require("../server/controllers/speechCron");

app.use(bodyParser.json({ limit: '30mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '30mb', extended: true }))

// Routes
app.use('/user', require('./routes/userRouter'))
app.use('/api', require('./routes/upload'))
app.use('/api', require('./routes/speechRoutes'))
app.use('/api', require('./routes/sentimentRoutes'))

//crone job start
// schedule.scheduleJob('*/2 * * * * *',(async function () {
//     try
//     {
//       await speechCrone();
//     }
//     catch (err)
//     {
//       console.log(err);
//     }
//   }));
//crone job end
// Connect to mongodb
const URI = process.env.MONGODB_URL
mongoose.connect(URI, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
}, err => {
    if(err) throw err;
    console.log("Connected to mongodb")
})

if(process.env.NODE_ENV === 'production'){
    app.use(express.static('client/build'))
    app.get('*', (req, res)=>{
        res.sendFile(path.join(__dirname, 'client', 'build', 'index.html'))
    })
}



const PORT = process.env.PORT || 5000
app.listen(PORT, () => {
    console.log('Server is running on port', PORT)
})
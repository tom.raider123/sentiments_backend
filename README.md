
## Install dependencies for server
### `npm install`

## Install dependencies for client
### cd client ---> `npm install`

## Connect to your mongodb and add info in .env

## Add your google client id and facebook app id in client/src/components/body/auth/Login.js

## Run the client & server with concurrently
### `npm run dev`

## Run the Express server only
### `npm run server`

## Run the React client only
### `npm run client`

### Server runs on http://localhost:5000 and client on http://localhost:3000

## export GOOGLE_APPLICATION_CREDENTIALS="./config/friendly-access-301813-44b150754175.json"
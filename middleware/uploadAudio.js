
module.exports = async function (req, res, next) {
  try {
    const file = req.body;
    if (!file) {
      return res.status(400).json({msg: "No file selected."});
    }
    next();
  } catch (err) {
    return res.status(500).json({ msg: err.message });
  }
};

const FilesModel = require("../models/filesModel");

module.exports = async function (req, res, next) {
    try {
        const { id } = req.body;
        const file = await FilesModel.findOne({ _id: id });
      next();
      return file;
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  };

module.exports = async function(req, res, next) {
    try {
        if(!req.file || Object.keys(req.file).length === 0)
            return res.status(400).json({msg: "No files were uploaded."})
        const file = req.file;
        if(file.size > 1024 * 1024){
            return res.status(400).json({msg: "Size too large."})
        } // 1mb

        if(file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/png'){
            return res.status(400).json({msg: "File format is incorrect."})
        }

        next()
    } catch (err) {
        return res.status(500).json({msg: err.message})
    }
}

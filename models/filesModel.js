const mongoose = require('mongoose')


const filesSchema = new mongoose.Schema({
    userId: {
        type: String,
    },
    batchId: {
        type: String,
        default: null
    },
    filename: {
        type: String,
        trim: true
    },
    audiourl: {
        type: String,
        trim: true
    },
    audiotype: {
        type: String,
        trim: true
    },
    texturl: {
        type: String,
        trim: true
    },
    isConverted: {
        type: Number,
        default: false
    },
    isAnalysed: {
        type: Number,
        default: false
    },
    sentiments: {
        type: Object,
        default: ""
    },
    isConverting: {
        type: Number,
        default: false
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("Files", filesSchema)
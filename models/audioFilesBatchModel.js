const mongoose = require('mongoose')

const batchSchema = new mongoose.Schema({
    batchName: {
        type: String,
        trim: true
    },
    userId: {
        type: String,
    },
    files: {
        type: Array,
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("AudioFilesBatch", batchSchema)
const mongoose = require('mongoose')


const speechToTextSchema = new mongoose.Schema({
    convertedAudioId: {
        type: String,
    },
}, {
    timestamps: true
})

module.exports = mongoose.model("speechToTextLog", speechToTextSchema)
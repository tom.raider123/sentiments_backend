const Users = require('../models/userModel')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const sendMail = require('./sendMail')
const {google} = require('googleapis')
const {OAuth2} = google.auth
const fetch = require('node-fetch')
const moment = require('moment')
const { Storage } = require('@google-cloud/storage');
const fs = require('fs');
const path = require('path');
const FilesModel = require('../models/filesModel')
const BatchModel = require('../models/audioFilesBatchModel');
const client = new OAuth2(process.env.MAILING_SERVICE_CLIENT_ID)
const {CLIENT_URL} = process.env
const storage = new Storage({
    keyFilename: path.join(__dirname,'../config/friendly-access-301813-0b5867b648ae.json'),
    projectId: 'friendly-access-301813'
  });

  const bucket = storage.bucket('tekzee');
const userCtrl = {
    register: async (req, res) => {
        try {
            const {firstName, lastName, email, password} = req.body
            if(!firstName || !firstName || !email || !password )
                return res.status(400).json({msg: "Please fill in all fields."})
            if(!validateEmail(email))
                return res.status(400).json({msg: "Invalid emails."})
            const user = await Users.findOne({email})
            if(user) return res.status(400).json({msg: "This email already exists."})
            if(password.length < 6)
                return res.status(400).json({msg: "Password must be at least 6 characters."})
            const passwordHash = await bcrypt.hash(password, 12)
            const newUser = {
                firstName, lastName, email, password: passwordHash
            }
            const activationToken = createActivationToken(newUser)
            const url = `${CLIENT_URL}/activate/${activationToken}`
            sendMail(email, url, "Verify your email address")
            res.json({msg: "Register Success! Please activate your email to start."})
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    activateEmail: async (req, res) => {
        try {
            const {activationToken} = req.body
            const user = jwt.verify(activationToken, process.env.ACTIVATION_TOKEN_SECRET)
            const {firstName, lastName, email, password} = user
            const check = await Users.findOne({email})
            console.log(check);
            if(check) return res.status(400).json({msg:"This email already exists."})
            const newUser = new Users({
                firstName, lastName, email, password
            })
            await newUser.save()
            res.json({msg: "Account has been activated!"})
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    login: async (req, res) => {
        try {
            const {email, password} = req.body
            const user = await Users.findOne({email})
            if(!user) return res.status(400).json({msg: "This email does not exist."})
            const isMatch = await bcrypt.compare(password, user.password)
            if(!isMatch) return res.status(400).json({msg: "Password is incorrect."})
            const refresh_token = createRefreshToken({id: user._id})
            console.log(refresh_token);
            res.cookie('refreshtoken', refresh_token, {
                httpOnly: true,
                path: '/user/refresh_token',
                maxAge: 7*24*60*60*1000 // 7 days
            })
            res.json({msg: "Login success!"});
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    getAccessToken: (req, res) => {
        try {
            const rf_token = req.cookies.refreshtoken
            if(!rf_token) return res.status(400).json({msg: "Please login now!"})
            jwt.verify(rf_token, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
                if(err) return res.status(400).json({msg: "Please login now!"})

                const access_token = createAccessToken({id: user.id})
                res.json({access_token})
            })
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    forgotPassword: async (req, res) => {
        try {
            const {email} = req.body
            const user = await Users.findOne({email})
            if(!user) return res.status(400).json({msg: "This email does not exist."})
            const access_token = createAccessToken({id: user._id})
            const url = `${CLIENT_URL}/user/reset/${access_token}`
            sendMail(email, url, "Reset your password")
            res.json({msg: "Re-send the password, please check your email."})
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    resetPassword: async (req, res) => {
        try {
            const {password} = req.body
            const passwordHash = await bcrypt.hash(password, 12)

            await Users.findOneAndUpdate({_id: req.user.id}, {
                password: passwordHash
            })

            res.json({msg: "Password successfully changed!"})
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    getUserInfor: async (req, res) => {
        try {
            const user = await Users.findById(req.user.id).select('-password')

            res.json(user)
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    getUsersAllInfor: async (req, res) => {
        try {
            const users = await Users.find().select('-password')

            res.json(users)
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    logout: async (req, res) => {
        try {
            res.clearCookie('refreshtoken', {path: '/user/refresh_token'})
            return res.json({msg: "Logged out."})
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    updateUser: async (req, res) => {
        try {
            const { firstName, lastName, email, phone, } = req.body
            await Users.findOneAndUpdate({_id: req.user.id}, {
                firstName, lastName, email, phone
            })
            res.json({msg: "Update Success!"})
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    updateAvatar: async (req, res) => {
        try {
            const { avatar } = req.body
            await Users.findOneAndUpdate({_id: req.user.id}, {
                avatar
            })
            res.json({msg: "Update Success!"})
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },

    updateUsersRole: async (req, res) => {
        try {
            const {role} = req.body

            await Users.findOneAndUpdate({_id: req.params.id}, {
                role
            })

            res.json({msg: "Update Success!"})
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    deleteUser: async (req, res) => {
        try {
            await Users.findByIdAndDelete(req.params.id)

            res.json({msg: "Deleted Success!"})
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    googleLogin: async (req, res) => {
        try {
            const {tokenId} = req.body

            const verify = await client.verifyIdToken({idToken: tokenId, audience: process.env.MAILING_SERVICE_CLIENT_ID})

            const {email_verified, email, name, picture} = verify.payload

            const password = email + process.env.GOOGLE_SECRET

            const passwordHash = await bcrypt.hash(password, 12)

            if(!email_verified) return res.status(400).json({msg: "Email verification failed."})

            const user = await Users.findOne({email})

            if(user){
                const isMatch = await bcrypt.compare(password, user.password)
                if(!isMatch) return res.status(400).json({msg: "Password is incorrect."})

                const refresh_token = createRefreshToken({id: user._id})
                res.cookie('refreshtoken', refresh_token, {
                    httpOnly: true,
                    path: '/user/refresh_token',
                    maxAge: 7*24*60*60*1000 // 7 days
                })

                res.json({msg: "Login success!"})
            }else{
                const newUser = new Users({
                    name, email, password: passwordHash, avatar: picture
                })

                await newUser.save()

                const refresh_token = createRefreshToken({id: newUser._id})
                res.cookie('refreshtoken', refresh_token, {
                    httpOnly: true,
                    path: '/user/refresh_token',
                    maxAge: 7*24*60*60*1000 // 7 days
                })

                res.json({msg: "Login success!"})
            }


        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    facebookLogin: async (req, res) => {
        try {
            const {accessToken, userID} = req.body

            const URL = `https://graph.facebook.com/v2.9/${userID}/?fields=id,name,email,picture&access_token=${accessToken}`

            const data = await fetch(URL).then(res => res.json()).then(res => {return res})

            const {email, name, picture} = data

            const password = email + process.env.FACEBOOK_SECRET

            const passwordHash = await bcrypt.hash(password, 12)

            const user = await Users.findOne({email})

            if(user){
                const isMatch = await bcrypt.compare(password, user.password)
                if(!isMatch) return res.status(400).json({msg: "Password is incorrect."})

                const refresh_token = createRefreshToken({id: user._id})
                res.cookie('refreshtoken', refresh_token, {
                    httpOnly: true,
                    path: '/user/refresh_token',
                    maxAge: 7*24*60*60*1000 // 7 days
                })

                res.json({msg: "Login success!"})
            }else{
                const newUser = new Users({
                    name, email, password: passwordHash, avatar: picture.data.url
                })

                await newUser.save()

                const refresh_token = createRefreshToken({id: newUser._id})
                res.cookie('refreshtoken', refresh_token, {
                    httpOnly: true,
                    path: '/user/refresh_token',
                    maxAge: 7*24*60*60*1000 // 7 days
                })

                res.json({msg: "Login success!"})
            }


        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    validateBatchNameFileUpload: async (req,res) => {
        try {
            const checkName = await BatchModel.find({batchName:req.body.batchName});
            if(checkName.length > 0){
                return res.json({ msg:"Batch name already been taken" })
            }
            return res.json({ msg: "Batch name available." })

        } catch (err) {
            res.status(400).json('Error: ' + err)
        }
    },
    userUpload: async (req,res) => {
        try {
            const { files, batchName } = req.body
            const newBatch =  await new BatchModel({
                batchName: batchName ? batchName : '',
                userId: req.params.id ? req.params.id : '',
            })
            const saveBatch = await newBatch.save();
            if(saveBatch){
                for (var i = 0; i < files.length; i++){
                    const newFiles = new FilesModel({
                        userId: req.params.id ? req.params.id : '',
                        batchId: saveBatch ? saveBatch._id:0,
                        filename: files[i].file.path ? files[i].file.path : '',
                        audiourl:files[i].url.url ? files[i].url.url : '',
                        audiotype:'',
                        texturl:''
                    })
                    await newFiles.save()
                }
                return res.status(200).json({msg:"Files uploaded successfully!"})
            }
            await storage.bucket(bucket).file(fileName).delete();
            return res.status(500).json({msg: "Please try again.."})
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    getUpload: async (req, res) => {
        try {
            const id = req.query.id || '';
            const files = await FilesModel.find({userId:id});
            res.json({ files });
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    getUserBatches: async (req, res) => {
        try {
            const id = req.params.id || '';
            const batches = await BatchModel.find({userId:id});
            if(batches){
                for (var i = 0; i < batches.length; i++){
                    const files = await FilesModel.find({batchId:batches[i]._id});
                    const converted = files.filter((data) => data.isConverted === 1);
                    const notConverted = files.filter((data) => data.isConverted === 0);
                    const analysed = files.filter((data) => data.isAnalysed === 1);
                    const notAnalysed = files.filter((data) => data.isAnalysed === 0);
                    const positive = files.filter((data) => data.sentiments.score > 0);
                    const negative = files.filter((data) => data.sentiments.score < 0);
                    const neutral = files.filter((data) => data.sentiments.score === 0);
                    filesData = {
                        converted: converted.length,
                        notConverted: notConverted.length,
                        analysed: analysed.length,
                        notAnalysed: notAnalysed.length,
                        positive: positive.length,
                        negative: negative.length,
                        neutral: neutral.length,
                    }
                    const datas = batches[i].files.push(filesData);
                }
                return res.status(200).json({batches});
            }
            return res.status(200).json({ batches })

        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    getBatchFiles: async (req, res) => {
        try {
            const id = req.params.id || '';
            const files = await FilesModel.find({batchId:id});
            res.json({ files });
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },
    userDashboard: async (req, res) => {
        try {
          const id = req.params.id;
          const today = moment().startOf('day')
          const total_audio = await BatchModel.find({userId:id});
          const today_uploads = await BatchModel.find({userId:id,
            createdAt: {
              $gte: today.toDate(),
              $lte: moment(today).endOf('day').toDate()
            }
          })
          const total_converted = await FilesModel.find({userId:id, texturl: { $ne: "" }});
          const total_analysis = await FilesModel.find({userId:id, sentiments: { $ne: "" }});
          dashboard = {
            totalAudioToday:today_uploads ? today_uploads.length: 0,
            totalAudio: total_audio ? total_audio.length : 0,
            totalConverted: total_converted ? total_converted.length : 0,
            totalAnalysis: total_analysis ? total_analysis.length : 0 ,
          }
          res.json(dashboard);
        }catch (err) {
        return res.status(500).json({msg: err.message})
    }

    }
}


function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

const createActivationToken = (payload) => {
    return jwt.sign(payload, process.env.ACTIVATION_TOKEN_SECRET, {expiresIn: '5m'})
}

const createAccessToken = (payload) => {
    return jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {expiresIn: '15m'})
}

const createRefreshToken = (payload) => {
    return jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET, {expiresIn: '7d'})
}




module.exports = userCtrl
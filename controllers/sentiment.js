const Sentiment = require('sentiment');

const FilesModel = require("../models/filesModel");

const textToSentiments = async (id) => {
    try {
        const file = await FilesModel.findOne({ _id: id });
        const text = file.texturl;
        console.log(file.texturl);
        var sentiment = new Sentiment();
        var ngLanguage = {
          labels: { 'प्लान': -1, 'तो': -1, 'अभी': +1, 'जी': +1,'कोई': +1}
        };
        sentiment.registerLanguage('hi', ngLanguage);
        var result = await sentiment.analyze(file.texturl,{ language: 'hi' });
        await FilesModel.findOneAndUpdate({_id: file._id}, {
            sentiments: result,
            isAnalysed: true
        })
      } catch (err) {
        return err.message;
      }
}
module.exports = textToSentiments;
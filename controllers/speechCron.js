const FilesModel = require("../models/filesModel");
const speechToTextCronLog = require("../models/speechToTextLog");
const speech  = require("@google-cloud/speech");

 const speechCron = async (req, res) => {
     try{
        const bucketName = 'tekzee';
        const file = await FilesModel.findOne({ isConverting: false });
        if(file && file.isConverting == false ){
          await FilesModel.findOneAndUpdate({_id: file._id}, {
            isConverting: true,
        });
            console.log('inside crone');
            const client = new speech.SpeechClient();
            const gcsUri = `gs://${bucketName}/${file.filename}`;
            const encoding = 'LINEAR16';
            const sampleRateHertz = 8000;
            const languageCode = 'hi-IN';

            const config = {
                encoding: encoding,
                sampleRateHertz: sampleRateHertz,
                languageCode: languageCode,
              };

              const audio = {
                uri: gcsUri,
              };

              const request = {
                config: config,
                audio: audio,
              };

              const [operation] = await client.longRunningRecognize(request);
              // Get a Promise representation of the final result of the job
              const [response] = await operation.promise();
              const transcription = response.results
              .map(result => result.alternatives[0].transcript)
              .join('\n');
              if(transcription){
                  await FilesModel.findOneAndUpdate({_id: file._id}, {
                      texturl: transcription,
                      isConverted: true
                  });
                  const newCron = new speechToTextCronLog({
                    convertedAudioId: file._id
                  })
                await newCron.save();
                return 'Converted successfully';
              }
        }
     }
     catch (err) {
         console.log(err.message);
      }
}
module.exports = speechCron;

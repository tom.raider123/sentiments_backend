const speech = require("@google-cloud/speech");
const fs = require("fs");
const FilesModel = require("../models/filesModel");
// const convertSpeechToText = require('./speech');
const speechToTextCronLog = require("../models/speechToTextLog");

const speechToTextCtrl = {
  convertToText: async (req, res) => {
    try {
      const { id } = req.body;
      const bucketName = 'tekzee';
      const file = await FilesModel.findOne({ _id: id });
      if(file.texturl == ""){
       //code start
       const client = new speech.SpeechClient();
        const gcsUri = `gs://${bucketName}/${file.filename}`;
        const encoding = 'LINEAR16';
        const sampleRateHertz = 8000;
        const languageCode = 'hi-IN';

        const config = {
            encoding: encoding,
            sampleRateHertz: sampleRateHertz,
            languageCode: languageCode,
          };

          const audio = {
            uri: gcsUri,
          };

          const request = {
            config: config,
            audio: audio,
          };
          const [operation] = await client.longRunningRecognize(request);
        // Get a Promise representation of the final result of the job
        const [response] = await operation.promise();
        const transcription = response.results
        .map(result => result.alternatives[0].transcript)
        .join('\n');
        if(response.results.length === 0){
          return res.status(400).json({ msg: 'Processed with no result' });
        }else{
            await FilesModel.findOneAndUpdate({_id: file._id}, {
                texturl: transcription,
                isConverted: true
            });
            const newCron = new speechToTextCronLog({
              convertedAudioId: file._id
            })
          await newCron.save();
          return res.status(200).json({ msg: 'Converted successfully' });
        }

      }
      return res.status(200).json({ msg: 'Already converted' });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },

};
module.exports = speechToTextCtrl;

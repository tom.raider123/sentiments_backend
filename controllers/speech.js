const speech = require('@google-cloud/speech');
const FilesModel = require("../models/filesModel");

const convertSpeechToText = async (file,bucketName) => {
    try {
        const client = new speech.SpeechClient();
        const gcsUri = `gs://${bucketName}/${file.filename}`;
        const encoding = 'LINEAR16';
        const sampleRateHertz = 8000;
        const languageCode = 'hi-IN';

        const config = {
            encoding: encoding,
            sampleRateHertz: sampleRateHertz,
            languageCode: languageCode,
          };

          const audio = {
            uri: gcsUri,
          };

          const request = {
            config: config,
            audio: audio,
          };
          const [operation] = await client.longRunningRecognize(request);
        // Get a Promise representation of the final result of the job
        const [response] = await operation.promise();
        console.log(response);
        const transcription = response.results
        .map(result => result.alternatives[0].transcript)
        .join('\n');
        console.log(transcription);
        await FilesModel.findOneAndUpdate({_id: file._id}, {
            texturl: transcription
        })
      } catch (err) {
        return err.message;
      }
}
module.exports = convertSpeechToText;
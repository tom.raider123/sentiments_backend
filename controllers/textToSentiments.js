var Sentiment = require('sentiment');
const fs = require("fs");
const FilesModel = require("../models/filesModel");
const textToSentiments = require('./sentiment');

const speechToTextCtrl = {
  convertToSentiments: async (req, res) => {
    try {
      const { id } = req.body;
      const file = await FilesModel.findOne({ _id: id });
      if(file.sentiments == ""){
        await textToSentiments(id);
        return res.status(200).json({ msg: 'Sentiment analysis successful' });
      }
      return res.status(200).json({ msg: 'Already analysed' });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getFile: async (req, res) => {
    try {
      const { id } = req.params;
      const file = await FilesModel.findOne({ _id: id });
      res.send({ score:file.sentiments.score,
        negative:file.sentiments.negative,
        positive:file.sentiments.positive,
        comparative:file.sentiments.comparative });
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
  getTranslation: async (req, res) => {
    try {
      const { id } = req.params;
      const translation = await FilesModel.findOne({ _id: id }).select('texturl');
      res.send(translation.texturl);
    } catch (err) {
      return res.status(500).json({ msg: err.message });
    }
  },
};
module.exports = speechToTextCtrl;

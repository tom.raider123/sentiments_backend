const cloudinary = require('cloudinary')
const { Storage } = require('@google-cloud/storage');
const fs = require('fs');
const path = require('path');
cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.CLOUD_API_KEY,
    api_secret: process.env.CLOUD_API_SECRET
})

const storage = new Storage({
    keyFilename: path.join(__dirname,'../config/friendly-access-301813-0b5867b648ae.json'),
    projectId: 'friendly-access-301813'
  });

  const bucket = storage.bucket('tekzee');

const uploadCtrl = {
    uploadAvatar: (req, res) => {
        try {
            image=req.file;
            let promises = [];
              const blob = bucket.file(image.originalname);
              const newPromise = new Promise((resolve, reject) => {
                blob.createWriteStream({
                    metadata: { contentType: image.mimetype },
                    resumable: false, //Good for small files
                  })
                  .on("finish", () => {
                    const Url = `https://storage.googleapis.com/${bucket.name}/${blob.name}`;
                    resolve({ url: Url });
                  })
                  .on("error", (err) => {
                    reject("upload error: ", err);
                  })
                  .end(image.buffer);
              });
              promises.push(newPromise);

            Promise.all(promises)
              .then((response) => {
                res.status(200).send(response[0]);
              })
              .catch((err) => {
                res.status(400).send(err.message[0]);
              });
        } catch (err) {
            return res.status(500).json({msg: err.message})
        }
    },

    uploadAudio: async (req, res) => {
        try{
            file=req.files[0];
            let promises = [];
              const blob = bucket.file(file.originalname);
              const newPromise = await new Promise((resolve, reject) => {
                blob.createWriteStream({
                    resumable: false, //Good for small files
                    // gzip: true,
                  })
                  .on("finish", () => {
                    const Url = `https://storage.googleapis.com/${bucket.name}/${blob.name}`;
                    resolve({ url: Url });
                  })
                  .on("error", (err) => {
                    reject("upload error: ", err);
                  })
                  .end(file.buffer);
              }).then((result) => {
                promises.push(result);
              }).catch( err => {
                return res.status(500).send({msg: err.message});
              });
            Promise.all(promises)
              .then((response) => {
                return res.status(200).send(response[0]);
              })
              .catch((err) => {
                return res.status(500).send(err.message[0]);
              });
        } catch (err){
            return res.status(500).json({msg: err.message})
        }
    },
    deleteFileFromBucket: async (req, res) =>  {
        await storage.bucket(bucketName).file(fileName).delete();
        console.log(`gs://${bucketName}/${fileName} deleted`);
      deleteFileFromBucket().catch(console.error);

    }

}

module.exports = uploadCtrl